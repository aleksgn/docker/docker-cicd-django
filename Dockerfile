FROM python:3-alpine

WORKDIR /django

COPY ./requirements.txt .

RUN pip install -r requirements.txt \
    && mkdir db

COPY . .

#VOLUME /django/db

EXPOSE 8000

CMD sh init.sh && python manage.py runserver 0.0.0.0:8000

